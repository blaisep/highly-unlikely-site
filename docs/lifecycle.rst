==============================
Lifecyle of a contribution
==============================


The process for extending or modifying this project is something like this:

Describe a scenario or feature
===============================

Create a branch for your change and start by describing the desired behavior in a feature file.

If you don't have a ``step`` to test it, just create a empty function in the step file, which a matching ``GIVEN`` clause and mark it as ``xdist`` so that CI will continue even when that test fails.

.. todo:: get some more clarity on this and provide an example of how to do it.
