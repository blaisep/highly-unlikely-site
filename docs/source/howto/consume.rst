==============================
Use Vitrina for your portfolio
==============================

Quick Start
============

If you're in a hurry and you just want to get a quick portfolio built, these steps should help you out:

	- Clone this repo
	- Edit the README and the content in the docs/folder to suit your project
	- ``make html``
	- push your html to wherever you host your documentation.

.. note:: These steps will build the doc. Stand by for more details


Minimal Instance
================

You should be able to clone this project, and run the `pypyr` pipeline, which will note any missing system requirements.
Once you have satisfied the system requirements, you can run the ansible role to install the project and run the conformance tests.

