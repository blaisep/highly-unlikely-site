===================================
Vitrina: a portfolio kit for devops
===================================


.. image:: _images/pipe.png
    :scale: 50
    :alt: A cartoon workman crawls out of a parody of Magritte's painting of a pipe.
    :align: center

TL;DR:
======

A kit for infrastructure developers to build portfolio apps that capture/format/publish test results and operational metrics.



.. toctree::
    :maxdepth: 1

    What is Vitrina? <describe/index>
    How to use it <howto/index>
    Who is involved? <contributors/index>
    Other Frequent Questions <describe/faq>
    reference/index


* :ref:`genindex` of Topics
* :ref:`glossary`
* :ref:`search`
