# highly-unlikely-com

## Placeholder portfolio site

More details at: [the docs hosted on readthedocs](https://vitrina.readthedocs.io/en/latest/about.html)

If you just want to grab and go, the structure of the project is intended to be:
    - HTML branch: build static HTML
    - Automation:  automation scripts for build, ship, run  (TODO)
    - Auth branch:  OAUTH for Google, Github, LinkedIn; but never FB (TODO)
    - Orchestration branch: rollback, failover, load balancing, canary builds, (TODO)

#### 30 sec summary

for now, this repo will be most useful if you `git clone` ...
and then
```bash
pip install -r docs/requirements-docs.txt
cd docs && sphinx-build -b html . _build/html
open _build/html/index.html
```

